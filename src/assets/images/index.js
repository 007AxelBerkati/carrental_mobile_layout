import Banner from './Banner.png';
import Allura from './Allura.png';
import Mercedes from './Mercedes.png';
import Splash from './Splash.png';
import HeaderBackground from './headerBackground.png';
import Axel from './Axel.png';

export {Banner, Splash, HeaderBackground, Axel, Mercedes, Allura};
