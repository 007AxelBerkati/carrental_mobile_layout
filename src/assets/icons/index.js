import IcHome from './home.svg';
import IcHomeActive from './homeActive.svg';
import IcList from './list.svg';
import IcListActive from './listActive.svg';
import IcUser from './user.svg';
import IcUserActive from './userActive.svg';
import IcMercedes from './mercedes.svg';
import IcBriefcase from './briefCase.svg';
import IcUsers from './users.svg';
import IcOlehOleh from './IcOlehOleh.svg';
import IcWisata from './IcWisata.svg';
import IcPenginapan from './IcPenginapan.svg';
import IcSewaMobil from './IcSewaMobil.svg';
import MercedesBanner from './MercedesBanner.svg';
import IcBg from './bg.svg';

export {
  IcHome,
  IcList,
  IcListActive,
  IcUser,
  IcUserActive,
  IcHomeActive,
  IcMercedes,
  IcBriefcase,
  IcUsers,
  IcOlehOleh,
  IcWisata,
  IcPenginapan,
  IcSewaMobil,
  MercedesBanner,
  IcBg,
};
